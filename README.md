## BUILD

To build the Dockerfile execute the command **docker build -t descricao:v1 .** in the command line.

---

## RUN

To run the image  execute the command **docker run --rm -it --name descricao descricao:v1** in the command lin