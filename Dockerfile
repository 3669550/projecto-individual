FROM alpine

LABEL maintainer="Monica Gonçalves"

RUN mkdir descricao

COPY descriçao.txt /descricao

WORKDIR /descricao

CMD [ "cat" , "/descricao/descriçao.txt" ]